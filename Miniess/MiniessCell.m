//
//  MiniessCell.m
//  miniess
//
//  Created by Santiago Bustamante on 8/31/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import "MiniessCell.h"
#import "MiniessImageViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Config.h"
#import <MediaPlayer/MediaPlayer.h>
#import "RTSpinKitView.h"
@interface MiniessCell ()
@property (weak, nonatomic) IBOutlet UIView *videoPlayView;
@property (weak, nonatomic) IBOutlet UIView *imgView;
@property (nonatomic, strong) RTSpinKitView *spinner;
@end
@implementation MiniessCell

- (void) setFrame:(CGRect)frame {
    [super setFrame:frame];
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self setNeedsDisplay];
}

-(void)setEntity:(MiniessMediaPagingEntity *)entity andIndexPath:(NSIndexPath *)index andThemeColor: (UIColor *)color{
//    [self.videoPlayView setHidden:YES];
    [self.videoPlayView setTag:10];
    [self.imgView setTag:10];
    self.isPlayingVideo = NO;
    self.spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:[UIColor whiteColor]];
    self.spinner.center = CGPointMake(self.videoPlayView.bounds.size.width/2, self.videoPlayView.bounds.size.height/2);
    self.spinner.hidesWhenStopped = YES;
    [self.videoPlayView addSubview:self.spinner];
    [self.videoPlayView bringSubviewToFront:self.spinner];
    [self.spinner startAnimating];
    [self setupCell: color];
    
    [self.imageButton setBackgroundImage:[UIImage imageNamed:@"miniessLoading.png"] forState:UIControlStateNormal];
    _entity = entity;
    
    MiniessMetaData *imgEntity = entity.mediaEntity.metaData;
    
    [imgEntity downloadImageWithBlock:^(UIImage *image, NSError *error) {
        if (self.indexPath.row == index.row) {
            [self.imageButton setBackgroundImage:image forState:UIControlStateNormal];
        }
    }];
    
    self.imageButton.userInteractionEnabled = YES;
    self.imageButton.center = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2);
    
    if (self.showOnePicturePerRow) {
        
        self.userLabel.text = self.entity.mediaEntity.username;
//        [self.contentView addSubview:self.userLabel];
        
        self.captionLabel.text = self.entity.mediaEntity.postTitle;
//        [self.contentView addSubview:self.captionLabel];
        
        [self.userImage setImage:[UIImage imageNamed:@"miniessLoading.png"]];
//        [self.contentView addSubview:self.userImage];
        
        self.tagLabel.text = self.entity.mediaEntity.tags;
        
        self.numOfLikeLabel.text = [NSString stringWithFormat:@"%d", self.entity.mediaEntity.numberLikes];
        
        self.numOfCommentLabel.text = [NSString stringWithFormat:@"%d", self.entity.mediaEntity.numberComment];
        
#warning profile image url
        NSString* urlAvatar = [NSString stringWithFormat:@"%@%@",@"avatar/.thumb/164_164_",entity.mediaEntity.avatarUrl];
        [MiniessModel downloadImageWithUrl:urlAvatar andBlock:^(UIImage *image2, NSError *error) {
            if (image2 && !error) {
                [self.userImage setImage:image2];
            }else{
                NSLog(@"ERROR: %@", error);
            }
        }];
    }else{
        [self.userLabel removeFromSuperview];
        [self.userImage removeFromSuperview];
        [self.captionLabel removeFromSuperview];
    }
//    [self playVideoForURL];
    [self initVideoPlay];
}

-(void) selectedImage:(id)selector{
    
    UIViewController *viewCon = (UIViewController *)self.nextResponder;
    
    while (![viewCon isKindOfClass:[UINavigationController class]]) {
        viewCon = (UIViewController *)viewCon.nextResponder;
    }
    
    MiniessImageViewController *img = [MiniessImageViewController imageViewerWithEntity:self.entity.mediaEntity];
    
    [((UINavigationController *)viewCon) pushViewController:img animated:YES];
    
}

- (void) setupCell: (UIColor *) color{
    
    [_imageButton addTarget:self action:@selector(selectedImage:) forControlEvents:UIControlEventTouchUpInside];
    
//    self.imageButton.contentMode = UIViewContentModeScaleAspectFit;
    
    
    UIColor* textColor = [CELL_COLORS objectAtIndex:(arc4random()%[CELL_COLORS count])];
    
    _userLabel.textColor = textColor;
    _tagLabel.backgroundColor = textColor;
    _captionLabel.textColor = textColor;
//    _numOfLikeLabel.textColor = textColor;
//    _numOfCommentLabel.textColor = textColor;
    _userLabel.font = RALE_WAY_REGULAR(15);
    _captionLabel.font = RALE_WAY_BOLD(30);
    _tagLabel.font = RALE_WAY_REGULAR(15);
    _numOfLikeLabel.font = RALE_WAY_REGULAR(15);
    _numOfCommentLabel.font = RALE_WAY_REGULAR(15);
    
    _userImage.contentMode = UIViewContentModeScaleAspectFit;
    _userImage.layer.masksToBounds = YES;
    _userImage.layer.cornerRadius = _userImage.bounds.size.width/2;
}

- (void) initVideoPlay {
    @try {
        [_videoPlayerView.view removeFromSuperview];
        _videoPlayerView = nil;
        _videoPlayerView = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[NSMutableString stringWithFormat:@"%@%@",MINIESS_RESOURCE_URL,self.entity.mediaEntity.metaData.videoUrl]]];
//        [_videoPlayerView.moviePlayer prepareToPlay];
        //
        _videoPlayerView.view.frame = self.videoPlayView.bounds;
        [self.videoPlayView addSubview:_videoPlayerView.view];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(playbackStateChanged)
                                                     name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
        [_videoPlayerView setScalingMode:MPMovieScalingModeAspectFill];
//        [_videoPlayerView.moviePlayer setControlStyle:MPMovieControlStyleNone];
        //    [_videoPlayerView.moviePlayer setFullscreen:true];
//        _videoPlayerView.moviePlayer.repeatMode = MPMovieRepeatModeOne;
        [_videoPlayerView setControlStyle:MPMovieControlStyleNone];
        [_videoPlayerView.view setUserInteractionEnabled:NO];
        [_videoPlayerView setRepeatMode:MPMovieRepeatModeOne];
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception);
    }
    @finally {
    }
}
- (void) playbackStateChanged {
    NSLog(@"%d",_videoPlayerView.playbackState);
    if(_videoPlayerView.playbackState == MPMoviePlaybackStatePlaying) {
        NSLog(@"is playing");

//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
//                                                 (unsigned long)NULL), ^(void) {
            [self.spinner stopAnimating];
            self.isPlayingVideo = YES;
//        });
    }
    
}
- (void) playVideo {
    [self.imgView setHidden:YES];
    [self.videoPlayView bringSubviewToFront:self.spinner];
    [self.videoPlayerView play];
}
- (void) pauseVideo {
    [self.imgView setHidden:NO];
    [self.videoPlayerView pause];
}
@end
