//
//  MiniessHTTPRequestOperationManager.m
//  miniess
//
//  Created by Santiago Bustamante on 8/26/13.
//  Copyright (c) 2013 busta117. All rights reserved.
//

#import "MiniessHTTPRequestOperationManager.h"
#import "MiniessController.h"

@implementation MiniessHTTPRequestOperationManager


+ (MiniessHTTPRequestOperationManager *)sharedManager {
    static MiniessHTTPRequestOperationManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[MiniessHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:@"http://developer.miniess.com"]];
    });
    
    return _sharedManager;
}


@end
