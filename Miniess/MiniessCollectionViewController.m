//
//  MiniessCollectionViewController.m
//  miniess
//
//  Created by Santiago Bustamante on 8/31/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import "MiniessCollectionViewController.h"
#import "MiniessController.h"
#import "MiniessMediaEntity.h"
#import "MiniessHTTPRequestOperationManager.h"
#import "MZRSlideInMenu.h"
#import "Config.h"
#import "RTSpinKitView.h"
#import <MBProgressHUD.h>
static NSString *const kButtonTitle0 = @"Likes";
static NSString *const kButtonTitle1 = @"Trending";
static NSString *const kButtonTitle2 = @"Popular";
static NSString *const kButtonTitle3 = @"Recent";
static NSString *const kButtonTitle4 = @" My Profile";
static NSString *const kButtonTitle5 = @"Settings";
@interface MiniessCollectionViewController()<MZRSlideInMenuDelegate>
{
    NSString *_strAPICallName;// Contains name of which API is calling,
    NSTimer *timer;
    MiniessCell *exCell;
    
}
@property (nonatomic, strong) NSMutableArray *mediaArray;
@property (nonatomic, strong) MiniessController *miniessController;
@property (nonatomic, assign) BOOL downloading;
@property (nonatomic, assign) BOOL hideFooter;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, assign) BOOL updating;
@property (nonatomic,strong) UIBarButtonItem *menuBarbtn;
@property (nonatomic, strong) BRFlabbyTableManager *flabbyTableManager;
@property (nonatomic, strong) RTSpinKitView *spinner;
@end

@implementation MiniessCollectionViewController

-(NSString *)version{
    return @"1.4";
}

//- (id) initWithCollectionViewLayout:(UICollectionViewLayout *)layout{
//    if ((self = [super initWithCollectionViewLayout:layout])) {
//        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [self.activityIndicator startAnimating];
//    }
//    return self;
//}

- (id) initWithStyle:(UITableViewStyle)style {
    if((self = [super initWithStyle:style])) {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.activityIndicator setHidesWhenStopped:YES];
        [self.activityIndicator stopAnimating];
    }
    return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    self.spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:[UIColor whiteColor]];
    self.spinner.center = CGPointMake([UIScreen mainScreen].bounds.size.width/2, [UIScreen mainScreen].bounds.size.height/2);
    self.spinner.hidesWhenStopped = YES;
    [[self.view superview] addSubview:self.spinner];
    [[self.view superview] bringSubviewToFront:self.spinner];
    
    self.title = @"miniess";
    self.downloading = YES;
    self.mediaArray = [NSMutableArray arrayWithCapacity:0];
    //trung cmt
//    [self.collectionView registerClass:[MiniessCell class] forCellWithReuseIdentifier:@"MiniessCell"];
//    [self.collectionView setBackgroundColor:[UIColor whiteColor]];
//    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footer"];

    [self.navigationController.navigationBar setTranslucent:NO];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.miniessController = [MiniessController miniessControllerWithMainViewController:self];
    self.miniessController.isSearchByTag = self.isSearchByTag;
    self.miniessController.searchTag = self.searchTag;
    [self downloadNext];
    
    
    
//    self.collectionView.alwaysBounceVertical = YES;
    refreshControl_ = [[MiniessRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl_ addTarget:self action:@selector(refreshCollection:) forControlEvents:UIControlEventValueChanged];
    loaded_ = YES;
    
//trung cmt
//    [self showSwitch];
    [self initMenuContent];
    [self setShowOnePicturePerRow:YES];
    
//    NSString *identifier = @"MiniessCell";
//    static BOOL nibMyCellloaded = NO;
//    if(!nibMyCellloaded)
//    {
//        UINib *nib = [UINib nibWithNibName:@"MiniessCell" bundle: nil];
//        [self.collectionView registerNib:nib forCellWithReuseIdentifier:identifier];
//        nibMyCellloaded = YES;
//    }
    
    self.flabbyTableManager = [[BRFlabbyTableManager alloc] initWithTableView:self.tableView];
    [self.flabbyTableManager setDelegate:self];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(checkScreen) userInfo:nil repeats:YES];
}


-(void) viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [super viewWillAppear:animated];
    
}

- (void) initMenuContent
{

    
//    UIButton *btnMenu = [[UIButton alloc] initWithFrame: CGRectMake(0, 0, 70, 44.0f)];
//    [btnMenu setTitle:@"||| Menu" forState:UIControlStateNormal];
//    [btnMenu setBackgroundColor:[UIColor yellowColor]];
//    [btnMenu addTarget:self action:@selector(fromLeftColorfulButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *barMenuBtn = [[UIBarButtonItem alloc] initWithCustomView:btnMenu];
//    
    _menuBarbtn = [[UIBarButtonItem alloc] initWithTitle:@"||| Menu" style:UIBarButtonItemStylePlain target:self action:@selector(fromLeftColorfulButtonTapped:)];
    
    self.navigationItem.leftBarButtonItem = _menuBarbtn;
    
}

-(void) segmentChanged:(id)sender{
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    
    if (self.showOnePicturePerRow != segmentedControl.selectedSegmentIndex) {
        self.showOnePicturePerRow = segmentedControl.selectedSegmentIndex;
    }
}


- (void) refreshCollection:(id) sender{
    [self.mediaArray removeAllObjects];
    [MiniessHTTPRequestOperationManager sharedManager].currentIndex = 0;
    [self downloadNext];
    if (self.activityIndicator.isAnimating) {
        [self.activityIndicator stopAnimating];
    }
}

- (void) downloadNext{
    __weak typeof(self) weakSelf = self;
    self.downloading = YES;
    [self.spinner startAnimating];
    // load new content
    if ([self.mediaArray count] == 0) {
        [self.miniessController mediaUserWithUserId:MINIESS_USER_ID andBlock:^(NSArray *mediaArray, NSError *error) {
            if ([refreshControl_ isRefreshing]) {
                [refreshControl_ endRefreshing];
            }
            if (error || mediaArray.count == 0) {
                SB_showAlert(@"miniess", @"No results found", @"OK");
                [weakSelf.activityIndicator stopAnimating];
            }else{
                [weakSelf.mediaArray addObjectsFromArray:mediaArray];
                [weakSelf.tableView reloadData];
            }
            weakSelf.downloading = NO;
        }];
    }else{
        // load more
        [self.miniessController mediaUserWithUserId:MINIESS_USER_ID andBlock:^(NSArray *mediaArray, NSError *error) {
           
            NSUInteger a = [self.mediaArray count];

            [weakSelf.mediaArray addObjectsFromArray:mediaArray];
            
            NSMutableArray *arr = [NSMutableArray arrayWithCapacity:0];

            [mediaArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                NSUInteger b = a+idx;
                NSIndexPath *path = [NSIndexPath indexPathForItem:b inSection:0];
                [arr addObject:path];
            }];
            
//            [_menuBarbtn setAction:nil];
            //trung cmt
//            [weakSelf.collectionView performBatchUpdates:^{
//                [weakSelf.collectionView insertItemsAtIndexPaths:arr];
//            } completion:^(BOOL finished) {
////                [_menuBarbtn setAction:@selector(fromLeftColorfulButtonTapped:)];
//            }];
            [self.tableView insertRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationAutomatic];
            
            weakSelf.downloading = NO;
            if (mediaArray.count == 0) {
                [weakSelf.activityIndicator stopAnimating];
                [weakSelf.tableView reloadData];
            }
            [weakSelf.activityIndicator stopAnimating];
            [weakSelf.spinner stopAnimating];
            
        }];
        
    }
    
}


- (void) setShowOnePicturePerRow:(BOOL)showOnePicturePerRow{
    BOOL reload = NO;
    if (_showOnePicturePerRow != showOnePicturePerRow) {
        reload = YES;
    }
    _showOnePicturePerRow = showOnePicturePerRow;
    if (reload && loaded_) {
        [self.tableView reloadData];
    }
    
}

- (void) setShowSwitchModeView:(BOOL)showSwitchModeView{
    _showSwitchModeView = showSwitchModeView;
    if (loaded_) {
        [self showSwitch];
    }

}

- (void) showSwitch{
    if (self.showSwitchModeView) {
        segmentedControl_ = [[UISegmentedControl alloc] initWithItems:@[[UIImage imageNamed:@"sb-grid-selected.png"],[UIImage imageNamed:@"sb-table-selected.png"]]];
        [self.view addSubview:segmentedControl_];
        
        segmentedControl_.segmentedControlStyle = UISegmentedControlStylePlain;
        CGRect frame = segmentedControl_.frame;
        frame.origin.y = 5;
        frame.size.width  = 200;
        frame.origin.x = self.view.center.x - 100;
        segmentedControl_.frame = frame;
        segmentedControl_.selectedSegmentIndex = _showOnePicturePerRow;
        [segmentedControl_ addTarget:self
                              action:@selector(segmentChanged:)
                    forControlEvents:UIControlEventValueChanged];
        
        frame = self.tableView.frame;
        frame.origin.y = CGRectGetMaxY(segmentedControl_.frame) + 5;
        frame.size.height = CGRectGetHeight([[UIScreen mainScreen] applicationFrame]) - CGRectGetMinY(frame);
        self.tableView.frame = frame;
        
    }else{
        if (segmentedControl_) {
            [segmentedControl_ removeFromSuperview];
            segmentedControl_ = nil;
        }
        
        CGRect frame = self.tableView.frame;
        frame.origin.y = 0;
        frame.size.height = CGRectGetHeight([[UIScreen mainScreen] applicationFrame]);
        self.tableView.frame = frame;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////
//#pragma mark - UICollectionViewDataSource
//
//- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
//    return [self.mediaArray count];
//}
///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark BRFlabbyTableManageDelegate
- (UIColor *)flabbyTableManager:(BRFlabbyTableManager *)tableManager flabbyColorForIndexPath:(NSIndexPath *)indexPath{
    
    return [self randomColor];
}

int indexOfOldColor = -1;
- (UIColor*) randomColor {
    NSArray *array = CELL_COLORS;
    int tmp;
    do {
        tmp = arc4random()%array.count;
    }while (tmp == indexOfOldColor);
    indexOfOldColor = tmp;
    return (UIColor*) array[tmp];
}
///////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UITableView Datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.mediaArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MiniessCell *cell = nil;
    if(cell==nil) {
        cell= (MiniessCell*) [[[NSBundle mainBundle] loadNibNamed:@"MiniessCell" owner:self options:nil] firstObject];
        @try {
            UIColor *themeColor = [self randomColor];
            [cell setFlabby:YES];
            [cell setLongPressAnimated:YES];
            [cell setFlabbyColor:themeColor];
            if ([self.mediaArray count]>0) {
                MiniessMediaPagingEntity *entity = [self.mediaArray objectAtIndex:indexPath.row];
                cell.indexPath = indexPath;
                cell.showOnePicturePerRow = self.showOnePicturePerRow;
                [cell setEntity:entity andIndexPath:indexPath andThemeColor:themeColor];
            }
            
            if (indexPath.row == [self.mediaArray count]-1 && !self.downloading) {
                [self.activityIndicator startAnimating];
                [self downloadNext];
            }
            
            
        }
        @catch (NSException *exception) {
            NSLog(@"____________%@",exception);
        }
        @finally {
            
        }
    }
    return cell;
}

#pragma mark UITableView Delegate
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *foot = [[UIView alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width, 20)];
    
    CGPoint center = self.activityIndicator.center;
    center.x = foot.center.x;
    center.y = 20;
    self.activityIndicator.center = center;
    
    [foot addSubview:self.activityIndicator];
    return foot;
}
-(CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
{
    return 30.0f;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(exCell==nil) {
        exCell = [[[NSBundle mainBundle] loadNibNamed:@"MiniessCell" owner:self options:nil] firstObject];
    }
    if (self.showOnePicturePerRow) {
        return exCell.frame.size.height;
    }else{
        if (SB_IS_IPAD) {
            return 200;
        }
        return 100;
    }
}

#pragma mark - UI animation Methonds.


- (void)fromLeftColorfulButtonTapped:(UIButton *)sender
{
    MZRSlideInMenu *menu = [[MZRSlideInMenu alloc] init];
    [menu setDelegate:self];
    [menu addMenuItemWithTitle:kButtonTitle0 textColor:[UIColor whiteColor] backgroundColor:[UIColor blackColor]];
    [menu addMenuItemWithTitle:kButtonTitle1 textColor:[UIColor whiteColor] backgroundColor:[UIColor colorWithWhite:0.7 alpha:1.0]];
    [menu addMenuItemWithTitle:kButtonTitle2 textColor:[UIColor colorWithWhite:0.7 alpha:1.0] backgroundColor:[UIColor yellowColor]];
    [menu addMenuItemWithTitle:kButtonTitle3 textColor:[UIColor whiteColor] backgroundColor:[UIColor purpleColor]];
    [menu addMenuItemWithTitle:kButtonTitle4 textColor:[UIColor whiteColor] backgroundColor:[UIColor redColor]];
    [menu showMenuFromLeft];
}

#pragma mark - Methods to call API of button selection basis.
/**
 *  For call respective API on basis on button click.
 *	@return: index as, identity of selected button.
 */
- (void)slideInMenu:(MZRSlideInMenu *)menuView didSelectAtIndex:(NSUInteger)index
{
    [MiniessHTTPRequestOperationManager sharedManager].currentIndex = 0;
    [MiniessHTTPRequestOperationManager sharedManager].lastIndex = 0;
    switch (index) {
        case 0:
            _strAPICallName = @"Photos";
            [MiniessHTTPRequestOperationManager sharedManager].currentAPI = API_VIDEO_10;
            [self.mediaArray removeAllObjects];
            [self.tableView reloadData];
            [self downloadNext];
            break;
            
        case 1:
            _strAPICallName = @"Trending Videos";
            [MiniessHTTPRequestOperationManager sharedManager].currentAPI = API_TrendingVideo_10;
            [self.mediaArray removeAllObjects];
            [self.tableView reloadData];

            [self downloadNext];
            break;
            
        case 2:
            _strAPICallName = @"Popular Videos";
            [MiniessHTTPRequestOperationManager sharedManager].currentAPI = API_PopularVideo_10;
                        [self.mediaArray removeAllObjects];
            [self.tableView reloadData];
            [self downloadNext];
            break;
            
        case 3:
            _strAPICallName = @"Recent Videos";
            [MiniessHTTPRequestOperationManager sharedManager].currentAPI = API_RecentVideo_10;
                        [self.mediaArray removeAllObjects];
                        [self.tableView reloadData];
            [self downloadNext];
            break;
            
        case 4:
            _strAPICallName = @"";
//            [self callSingleViewVideo];
            break;
            
        default:
            break;
    }
    NSLog(@"%@",_strAPICallName);
}


- (void) checkScreen {
    for(UITableViewCell *cell in [self.tableView visibleCells])
    {
        MiniessCell *ccell = (MiniessCell*)cell;
        UIView * v = [ccell viewWithTag:10];
        CGRect ccellRect = [[UIApplication sharedApplication].delegate.window convertRect:v.frame fromView:ccell];
        int off = 200;
        if([UIScreen mainScreen].bounds.size.height > 500) {
            off = 100;
        }
        if(ccellRect.origin.y> -10 &&  ccellRect.origin.y + ccell.frame.size.height < [UIScreen mainScreen].bounds.size.height+off)
        {
            int row = [[self.tableView indexPathForCell:ccell] row];
            NSLog(@"VV: %d", row);
            //            [ccell playVideo];
            if(!ccell.videoPlayerView.playbackState == MPMoviePlaybackStatePlaying) {
                [ccell playVideo];
            }
        }else{
            if(ccell.videoPlayerView.playbackState == MPMoviePlaybackStatePlaying) {
                [ccell pauseVideo];
            }
        }
    }
}
@end
