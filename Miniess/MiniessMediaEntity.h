//
//  MiniessMediaEntity.h
//  miniess
//
//  Created by Santiago Bustamante on 8/29/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MiniessMetaData : NSObject


@property (nonatomic, strong) NSString *thumburl;
@property (nonatomic, strong) NSString *videoUrl;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) UIImage *image;

+ (id) entityWithDictionary:(NSDictionary *)dictionary;
- (void) downloadImageWithBlock:(void (^)(UIImage *image, NSError * error))block;

@end

@interface MiniessUserEntity : NSObject
{
    
}

@property (nonatomic,strong) NSString* username;
@property (nonatomic,strong) NSString* email;
@property (nonatomic,strong) NSString* status;
@property (nonatomic,strong) NSString* photoUrl;
@property (nonatomic,strong) UIImage* image;
+ (id) entityWithDataDictionary :(NSDictionary *)dataDictionary;
//+ (void) downloadImageWithBlock:(void (^)(UIImage *image, NSError * error))block;
+ (void) entityWithID:(NSString *)userid andBlock:(void(^)(MiniessUserEntity* entity))block;

@end

@interface MiniessMediaEntity : NSObject

@property (nonatomic,strong) NSString *entityId;
@property (nonatomic,strong) NSString *postTitle;
@property (nonatomic,strong) NSString *postContent;
@property (nonatomic,strong) NSString *createdby;
@property (nonatomic,strong) NSString *tags;
@property (strong,nonatomic) MiniessUserEntity *userInfo;
@property (nonatomic,strong) MiniessMetaData *metaData;
@property (nonatomic,strong) NSString *shortinfo;
@property (nonatomic,strong) NSString *avatarUrl;
@property (nonatomic, assign) int numberComment;
@property (nonatomic, assign) int numberLikes;
@property (nonatomic,strong) NSString *username;

+ (id) entityWithDictionary:(NSDictionary *)dictionary;

@end




@interface MiniessMediaPagingEntity : NSObject

@property (nonatomic, strong) MiniessMediaEntity *mediaEntity;
@property (nonatomic, strong) NSString *nextUrl;

+ (id) entityWithDataDictionary:(NSDictionary *)dataDictionary andPagingDictionary:(NSDictionary *)pagingDictionary;

@end


