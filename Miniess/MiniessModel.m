//
//  MiniessModel.m
//  miniess
//
//  Created by Santiago Bustamante on 8/26/13.
//  Copyright (c) 2013 Busta. All rights reserved.
//

#import "MiniessModel.h"
#import "MiniessController.h"
#import "MiniessMediaEntity.h"

@implementation MiniessModel


+ (void) setIsSearchByTag:(BOOL) isSearchByTag{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setBool:isSearchByTag forKey:@"instagram_isSearchByTag"];
    [def synchronize];
}

+ (BOOL) isSearchByTag{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    return [def boolForKey:@"instagram_isSearchByTag"];
}

+ (void) setSearchTag:(NSString *)searchTag{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:searchTag forKey:@"instagram_searchTag"];
    [def synchronize];
}

+ (NSString *)searchTag{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    return [def objectForKey:@"instagram_searchTag"];
}

+ (void) checkminiessAccesTokenWithBlock:(void (^)(NSError * error))block{
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSString *token = [def objectForKey:@"instagram_access_token"];
    
    if (!token) {
        token = MINIESS_DEFAULT_ACCESS_TOKEN;
        [def setObject:token forKey:@"instagram_access_token"];
        [def synchronize];
    }
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithCapacity:0];
    [params setObject:token forKey:@"access_token"];
    
    [[MiniessHTTPRequestOperationManager sharedManager] GET:[NSString stringWithFormat:@"users/%@",MINIESS_USER_ID?:@""] parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        if (block){
            block(nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block){
            block(error);
        }
    }];
}


+ (void) mediaUserWithUserId:(NSString *)userId andBlock:(void (^)(NSArray *mediaArray, NSError * error))block{
    
//    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
//    NSString *token = [def objectForKey:@"instagram_access_token"];
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithCapacity:0];
//    [params setObject:token forKey:@"access_token"];
    [MiniessHTTPRequestOperationManager sharedManager].lastIndex = [MiniessHTTPRequestOperationManager sharedManager].currentIndex;
    int limit = [MiniessHTTPRequestOperationManager sharedManager].currentIndex +12;
    [MiniessHTTPRequestOperationManager sharedManager].currentIndex = limit;
    [params setObject:[NSString stringWithFormat:@"%d",limit] forKey:@"limit"];
//    [params setObject:[NSString stringWithFormat:@"%d",currentIndex] forKey:@"offset"];

    NSString *path = [MiniessHTTPRequestOperationManager sharedManager].currentAPI;
    if (path == nil ) {
        path = API_VIDEO_10;
    }
    
//    
//    if (MiniessModel.isSearchByTag && [MiniessModel searchTag].length > 0) {
//        path = [NSString stringWithFormat:@"tags/%@/media/recent",[MiniessModel searchTag]];
//    }
    
    [[MiniessHTTPRequestOperationManager sharedManager] GET:path parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSArray *mediaArr = responseObject;
//        NSDictionary *paging = responseObject[@"pagination"];
        
        __block NSMutableArray *mediaArrEntities = [NSMutableArray arrayWithCapacity:0];
        
        [mediaArr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            MiniessMediaPagingEntity *media = [MiniessMediaPagingEntity entityWithDataDictionary:obj andPagingDictionary:nil];
            [mediaArrEntities addObject:media];
        }];
        if (block) {
            int firstIndex =[MiniessHTTPRequestOperationManager sharedManager].lastIndex;
            NSRange range = NSMakeRange(firstIndex, [mediaArrEntities count]-firstIndex);
            
            mediaArrEntities = [NSMutableArray arrayWithArray:[mediaArrEntities subarrayWithRange:range]];
            block(mediaArrEntities,nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block){
            block(nil, error);
        }
    }];
    
    
    
}


+ (void) mediaUserWithPagingEntity:(MiniessMediaPagingEntity *)entity andBlock:(void (^)(NSArray *mediaArray, NSError * error))block{

    
    
    NSString *path = [entity.nextUrl stringByReplacingOccurrencesOfString:[[MiniessHTTPRequestOperationManager sharedManager].baseURL absoluteString] withString:@""];
    NSLog(@"%@",[MiniessHTTPRequestOperationManager sharedManager].baseURL);
    
    if (!path) {
        block(@[],nil);
        return;
    }
    
    [[MiniessHTTPRequestOperationManager sharedManager] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *mediaArr = responseObject;
        NSDictionary *paging = nil;
        
        __block NSMutableArray *mediaArrEntities = [NSMutableArray arrayWithCapacity:0];
        
        [mediaArr enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            MiniessMediaPagingEntity *media = [MiniessMediaPagingEntity entityWithDataDictionary:obj andPagingDictionary:paging];
            [mediaArrEntities addObject:media];
        }];
        if (block) {
            block(mediaArrEntities,nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block){
            block(nil, error);
        }
    }];

}



+ (void) downloadImageWithUrl:(NSString *)url andBlock:(void (^)(UIImage *image, NSError * error))block{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",MINIESS_RESOURCE_URL,url]]];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setResponseSerializer:[AFImageResponseSerializer serializer]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        if (responseObject) {
            if (block) {
                block(responseObject, error);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (block) {
            block(nil, error);
        }
    }];
    
    [operation start];
    
}
//http://developer.miniess.com/api/users/profile.json/stih9w3f8q4ieus2daow6jugwpen5ws5/1
+(void) userInfoWithId:(NSString*)userId andBlock:(void (^)(MiniessUserEntity* userInfo))block{
    
    
}


@end
