//
//  MiniessController.h
//  miniess
//
//  Created by Santiago Bustamante on 8/26/13.
//  Copyright (c) 2013 Busta. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MiniessMediaPagingEntity;
@class MiniessCollectionViewController;

#warning setup your application information here

static NSString *const miniess_API_VERSION = @"1";
//client information (from www.miniess.com/developer)
static NSString *const MINIESS_REDIRECT_URI = @"http://www.santiagobustamante.info";
static NSString *const MINIESS_CLIENT_SECRET = @"";
static NSString *const MINIESS_CLIENT_ID  = @"";

static NSString *const MINIESS_CLIENT_API_KEY= @"stih9w3f8q4ieus2daow6jugwpen5ws5";
static NSString *const MINIESS_RESOURCE_URL = @"http://miniess.com/uploads/";

static NSString *const API_VIDEO_10 = @"/api/videos/recent_videos.json/stih9w3f8q4ieus2daow6jugwpen5ws5";
static NSString *const API_TrendingVideo_10 = @"/api/videos/trending_video.json/stih9w3f8q4ieus2daow6jugwpen5ws5";
static NSString *const  API_PopularVideo_10 = @"/api/videos/popular_videos.json/stih9w3f8q4ieus2daow6jugwpen5ws5";
static NSString *const  API_RecentVideo_10 = @"/api/videos/recent_videos.json/stih9w3f8q4ieus2daow6jugwpen5ws5";


//if this value is empty or this token is expired or not valid, automatically request a new one. (if you set nil the app crash)
//static NSString *const miniess_DEFAULT_ACCESS_TOKEN  = @"";
static NSString *const MINIESS_DEFAULT_ACCESS_TOKEN  = @"blank";

static NSString *const MINIESS_USER_ID  = @"1"; //user id to requests



@interface MiniessController : NSObject

@property (nonatomic, assign) UIViewController *viewController;
@property (nonatomic, assign) BOOL isSearchByTag;
@property (nonatomic, strong) NSString *searchTag;

+ (MiniessCollectionViewController *) miniessViewController;
+ (id) miniessControllerWithMainViewController:(UIViewController *) viewController;
- (NSString *) AccessToken;
- (void) mediaUserWithUserId:(NSString *)userId andBlock:(void (^)(NSArray *mediaArray, NSError * error))block;
- (void) mediaUserWithPagingEntity:(MiniessMediaPagingEntity *)entity andBlock:(void (^)(NSArray *mediaArray, NSError * error))block;


@end
