//
//  MiniessHTTPRequestOperationManager.h
//  miniess
//
//  Created by Santiago Bustamante on 8/26/13.
//  Copyright (c) 2013 busta117. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface MiniessHTTPRequestOperationManager : AFHTTPRequestOperationManager
@property (assign) int currentIndex;
@property (assign) int lastIndex;
@property (strong, nonatomic) NSString* currentAPI;
+ (MiniessHTTPRequestOperationManager *)sharedManager;

@end
