//
//  MiniessImageViewController.m
//  MedellinHipHop
//
//  Created by Santiago Bustamante on 9/2/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import "MiniessImageViewController.h"
#import <MediaPlayer/MediaPlayer.h>


@interface MiniessImageViewController ()
@property(strong, nonatomic) MPMoviePlayerController *player;
@property(strong, nonatomic) MPMoviePlayerViewController *videoPlayerView;
@end

@implementation MiniessImageViewController


+ (id) imageViewerWithEntity:(MiniessMediaEntity *)entity{
    MiniessImageViewController *instance = [[MiniessImageViewController alloc] initWithNibName:@"MiniessImageViewController" bundle:nil];
    instance.entity = entity;
    return instance;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    __weak typeof(self) weakSelf = self;
    CGRect frame = self.imageView.frame;
//    frame.origin = CGPointZero;
    frame.size = CGSizeMake(CGRectGetWidth([[UIScreen mainScreen] applicationFrame]), CGRectGetWidth([[UIScreen mainScreen] applicationFrame]));
    [self.imageView setFrame:frame];
    
    self.containerView.center = self.view.center;
    
    
    
    self.title = @"";
    
    MiniessMetaData *picEntity = self.entity.metaData;
    
    [MiniessModel downloadImageWithUrl:picEntity.thumburl andBlock:^(UIImage *image, NSError *error) {
        [weakSelf.activityIndicator stopAnimating];
        if (image && !error) {
            [weakSelf.imageView setImage:image];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Something is wrong" message:@"Please check your Internet connection and try again later" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    
    [weakSelf.imageView addGestureRecognizer:tapGesture];
    weakSelf.imageView.userInteractionEnabled = YES;
    

    self.captionLabel.text = self.entity.postTitle;
    frame = self.captionLabel.frame;
    frame.size.width = CGRectGetWidth([[UIScreen mainScreen] applicationFrame]);
    frame.origin.y = CGRectGetMaxY(self.imageView.frame);
    self.captionLabel.frame = frame;
    
    self.activityIndicator.center = self.imageView.center;
    
    
    self.userLabel = [[UILabel alloc] init];
    self.userLabel.frame = CGRectMake(CGRectGetMinX(self.imageView.frame) + 55, CGRectGetMinY(self.imageView.frame) - 45, CGRectGetWidth(self.imageView.frame) - 65, 35);
    self.userLabel.textColor = [UIColor blackColor];
    self.userLabel.backgroundColor = [UIColor clearColor];
    self.userLabel.font = [self.userLabel.font fontWithSize:12];
    
    self.userImage = [[UIImageView alloc] init];
    self.userImage.frame = CGRectMake(CGRectGetMinX(self.imageView.frame) + 10, CGRectGetMinY(self.imageView.frame) - 45, 35, 35);
    self.userImage.contentMode = UIViewContentModeScaleAspectFit;
    self.userImage.layer.masksToBounds = YES;
    self.userImage.layer.cornerRadius = 17.5;
    
    self.userLabel.text = self.entity.createdby;
    [self.imageView.superview addSubview:self.userLabel];
    
    [self.userImage setImage:[UIImage imageNamed:@"miniessLoading.png"]];
    [self.imageView.superview addSubview:self.userImage];
    
    
    NSString* urlAvatar = [NSString stringWithFormat:@"%@%@",@"avatar/.thumb/",self.entity.avatarUrl];
    [MiniessModel downloadImageWithUrl:urlAvatar andBlock:^(UIImage *image2, NSError *error) {
        if (image2 && !error) {
            [weakSelf.userImage setImage:image2];
        }
    }];
    
}


#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImageView:nil];
    [super viewDidUnload];
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    NSLog(@"play %@", _entity.metaData.videoUrl);
//    [self playVideoForURL:_entity.metaData.videoUrl];
//}

- (void)tapAction
{
    NSLog(@"play %@", _entity.metaData.videoUrl);
    [self playVideoForURL:_entity.metaData.videoUrl];
}


#pragma mark - Video Playing Methods.
-(void) playVideoForURL: (NSString *)strURLForVideo
{
    _videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[NSMutableString stringWithFormat:@"%@%@",MINIESS_RESOURCE_URL,strURLForVideo]]];
    _videoPlayerView.view.frame = self.imageView.frame;
//    [self.navigationController presentMoviePlayerViewControllerAnimated:_videoPlayerView];
    [self.view addSubview:_videoPlayerView.view];
    
    //Set this if you wants to set full screen as size of your view.
    //[_videoPlayerView setWantsFullScreenLayout:NO];
    
    //Set this if you wants to see full page video.
    //[self presentMoviePlayerViewControllerAnimated:_videoPlayerView];
    
    //Set this if you wants to see complte video each time, means to remove options.
    // [_videoPlayerView.moviePlayer setControlStyle:MPMovieControlStyleNone];
    
//    [_videoPlayerView.moviePlayer setScalingMode:MPMovieScalingModeAspectFill];
    [_videoPlayerView.moviePlayer setControlStyle:MPMovieControlStyleNone];
//    [_videoPlayerView.moviePlayer setFullscreen:true];
    
    [[NSNotificationCenter defaultCenter]addObserver:self
     
                                            selector:@selector(movieFinishedCallback:)
                                                name:MPMoviePlayerPlaybackDidFinishNotification
                                              object:_player];
    
    [_videoPlayerView.moviePlayer play];
}

- (void) movieFinishedCallback:(NSNotification*)notification {
    
    MPMoviePlayerController *moviePlayer = [notification object];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
    
    //    if ([moviePlayer respondsToSelector:@selector(setFullscreen:animated:)]) {
    //        [moviePlayer.view removeFromSuperview];
    //    }
}

@end
