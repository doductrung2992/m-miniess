//
//  MiniessMediaEntity.m
//  miniess
//
//  Created by Santiago Bustamante on 8/29/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import "MiniessMediaEntity.h"
#import "MiniessModel.h"

@implementation MiniessMediaEntity

+ (id) entityWithDictionary:(NSDictionary *)dictionary{
//    @property (nonatomic,strong) NSString *entityId;
//    @property (nonatomic,strong) NSString *postTitle;
//    @property (nonatomic,strong) NSString *postContent;
//    @property (nonatomic,strong) NSString *createdby;
//    @property (nonatomic,strong) NSString *videoUrl;
//    @property (nonatomic,strong) NSString *thumbUrl;
//    @property (nonatomic,strong) NSString *shortinfo;
//    @property (nonatomic,strong) NSString *photoURL;
    NSLog(@"%@", dictionary);
    MiniessMediaEntity *entity = [[MiniessMediaEntity alloc] init];
    entity.entityId = dictionary[@"id"];
    entity.postTitle = dictionary[@"post_title"];
    entity.postContent = dictionary[@"post_content"];
//    entity.userInfo = [MiniessUserEntity entityWithDataDictionary:dictionary];
    entity.metaData = [MiniessMetaData entityWithDictionary:dictionary];
    entity.shortinfo = dictionary[@"shortinfo"];
    entity.createdby = dictionary[@"createby"];
    entity.avatarUrl = dictionary[@"photoURL"];
    @try {
        entity.numberComment = (dictionary[@"comments"]==nil?0:[dictionary[@"comments"] integerValue]);
        entity.numberLikes = (dictionary[@"like"]==nil?0:[dictionary[@"like"] integerValue]);
    }
    @catch (NSException *exception) {
        entity.numberComment = 0;
        entity.numberLikes = 0;
    }
    entity.username = dictionary[@"username"];
    
    entity.tags = dictionary[@"tags"];
    return entity;
}

//
//+ (NSMutableDictionary *)imagesWithDictionary: (NSMutableDictionary *)dictionary{
//    NSMutableDictionary *retVal = [NSMutableDictionary dictionaryWithCapacity:0];
//    [retVal setObject:[MiniessMetaData entityWithDictionary:dictionary[@"low_resolution"]] forKey:@"low_resolution"];
//    [retVal setObject:[MiniessMetaData entityWithDictionary:dictionary[@"standard_resolution"]] forKey:@"standard_resolution"];
//    [retVal setObject:[MiniessMetaData entityWithDictionary:dictionary[@"thumbnail"]] forKey:@"thumbnail"];
//    return retVal;
//}


@end


@implementation MiniessMetaData

+ (id) entityWithDictionary:(NSDictionary *)dictionary{
    
    NSString* metaData = dictionary[@"meta_value"];
    NSError *error;
    NSData *jsonData = [metaData dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dictTemp1 = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    
    MiniessMetaData *entity = [[MiniessMetaData alloc] init];
    
    entity.thumburl = [dictTemp1 objectForKey:@"thumburl"];
    entity.videoUrl = [dictTemp1 objectForKey:@"videourl"];
    entity.type = [dictTemp1 objectForKey:@"filetype"];

//    [entity downloadImageWithBlock:nil];
    
    return entity;
}


- (void) downloadImageWithBlock:(void (^)(UIImage *image, NSError * error))block{
    
    [MiniessModel downloadImageWithUrl:self.thumburl andBlock:^(UIImage *image2, NSError *error) {
        if (image2 && !error) {
            
            if (block) {
                block(image2, nil);
            }
        }
    }];
}



@end

@implementation MiniessMediaPagingEntity

+ (id) entityWithDataDictionary:(NSDictionary *)dataDictionary andPagingDictionary:(NSDictionary *)pagingDictionary{
    MiniessMediaPagingEntity *entity = [[MiniessMediaPagingEntity alloc] init];
    entity.nextUrl = pagingDictionary[@"next_url"];
    entity.mediaEntity = [MiniessMediaEntity entityWithDictionary:dataDictionary];
    
    return entity;
}

@end


@implementation MiniessUserEntity
+ (void) entityWithID:(NSString *)userid andBlock:(void(^)(MiniessUserEntity* entity))block;
{
    MiniessUserEntity *entity;
//    NSString *userId = dataDictionary[@"createdby"];
    //http://developer.miniess.com/api/users/profile.json/stih9w3f8q4ieus2daow6jugwpen5ws5/1
    
    NSString *path = [NSString stringWithFormat:@"api/users/profile.json/stih9w3f8q4ieus2daow6jugwpen5ws5/%@",userid?:@""];
    
    [[MiniessHTTPRequestOperationManager sharedManager] GET:path parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary* info = responseObject;
        entity.username = info[@"username"];
        entity.email = info[@"email"];
        entity.status = info[@"status"];
        entity.photoUrl = info[@"photoURL"];
        if (entity) {
            block(entity);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"fetch user info false");
    }];
    
}

+ (id) entityWithDataDictionary :(NSDictionary *)dataDictionary
{
    MiniessUserEntity *entity = [MiniessUserEntity new];
    NSString *userId = dataDictionary[@"createdby"];
    //http://developer.miniess.com/api/users/profile.json/stih9w3f8q4ieus2daow6jugwpen5ws5/1
    
    NSString *path = [NSString stringWithFormat:@"http://developer.miniess.com/api/users/profile.json/stih9w3f8q4ieus2daow6jugwpen5ws5/%@",userId?:@""];
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:path]];
    NSError *error=nil;
    NSDictionary *info=[NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error:&error];
    
    entity.username = info[@"username"];
    entity.email = info[@"email"];
    entity.status = info[@"status"];
    entity.photoUrl = info[@"photoURL"];
    return entity;
}

@end

