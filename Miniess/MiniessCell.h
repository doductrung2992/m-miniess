//
//  MiniessCell.h
//  miniess
//
//  Created by Santiago Bustamante on 8/31/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MiniessMediaEntity.h"
#import <BRFlabbyTableViewCell.h>
#import <MediaPlayer/MediaPlayer.h>
@interface MiniessCell : BRFlabbyTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *tagLabel;

@property (strong, nonatomic) IBOutlet UILabel *userLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *captionLabel;
@property (strong, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet UILabel *numOfLikeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numOfCommentLabel;
@property (assign, nonatomic) MiniessMediaPagingEntity *entity;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, assign) BOOL showOnePicturePerRow;
@property (nonatomic, assign) BOOL isPlayingVideo;
@property(strong, nonatomic) MPMoviePlayerController *videoPlayerView;

-(void)setEntity:(MiniessMediaPagingEntity *)entity andIndexPath:(NSIndexPath *)index andThemeColor: (UIColor *)color;
- (void) playVideo;
- (void) pauseVideo;
@end
