//
//  MiniessMediaEntity.h
//  instagram
//
//  Created by Santiago Bustamante on 8/29/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

enum MiniessMediaType {
    MiniessMediaTypeImage = 0,
    MiniessMediaTypeVideo
    };

@interface MiniessMediaEntity : NSObject

@property (nonatomic, assign) enum MiniessMediaType type;
@property (nonatomic, strong) NSMutableDictionary *images;
@property (nonatomic, strong) NSString *caption;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *profilePicture;

+ (id) entityWithDictionary:(NSDictionary *)dictionary;

@end


@interface MiniessImageEntity : NSObject

@property (nonatomic, strong) NSString *url;
@property (nonatomic, assign) int height;
@property (nonatomic, assign) int width;
@property (nonatomic, strong) UIImage *image;

+ (id) entityWithDictionary:(NSDictionary *)dictionary;
- (void) downloadImageWithBlock:(void (^)(UIImage *image, NSError * error))block;

@end


@interface MiniessMediaPagingEntity : NSObject

@property (nonatomic, strong) MiniessMediaEntity *mediaEntity;
@property (nonatomic, strong) NSString *nextUrl;

+ (id) entityWithDataDictionary:(NSDictionary *)dataDictionary andPagingDictionary:(NSDictionary *)pagingDictionary;

@end
