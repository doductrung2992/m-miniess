//
//  MiniessHTTPRequestOperationManager.m
//  instagram
//
//  Created by Santiago Bustamante on 8/26/13.
//  Copyright (c) 2013 busta117. All rights reserved.
//

#import "MiniessHTTPRequestOperationManager.h"
#import "MiniessController.h"

@implementation MiniessHTTPRequestOperationManager


+ (MiniessHTTPRequestOperationManager *)sharedManager {
    static MiniessHTTPRequestOperationManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[MiniessHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://api.instagram.com/v%@/",INSTAGRAM_API_VERSION]]];
    });
    
    return _sharedManager;
}


@end
