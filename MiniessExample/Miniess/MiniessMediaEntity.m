//
//  MiniessMediaEntity.m
//  instagram
//
//  Created by Santiago Bustamante on 8/29/13.
//  Copyright (c) 2013 Pineapple Inc. All rights reserved.
//

#import "MiniessMediaEntity.h"
#import "MiniessModel.h"

@implementation MiniessMediaEntity

+ (id) entityWithDictionary:(NSDictionary *)dictionary{
    
    MiniessMediaEntity *entity = [[MiniessMediaEntity alloc] init];
    entity.type = [dictionary[@"type"] isEqualToString:@"image"]? MiniessMediaTypeImage : MiniessMediaTypeVideo;
    entity.caption = (![dictionary[@"caption"] isKindOfClass:[NSNull class]])?(dictionary[@"caption"])[@"text"]:@"";
    entity.images = [MiniessMediaEntity imagesWithDictionary:dictionary[@"images"]];
    entity.userName = dictionary[@"user"][@"username"];
    entity.profilePicture = dictionary[@"user"][@"profile_picture"];

    
    return entity;
}


+ (NSMutableDictionary *)imagesWithDictionary: (NSMutableDictionary *)dictionary{
    NSMutableDictionary *retVal = [NSMutableDictionary dictionaryWithCapacity:0];
    [retVal setObject:[MiniessImageEntity entityWithDictionary:dictionary[@"low_resolution"]] forKey:@"low_resolution"];
    [retVal setObject:[MiniessImageEntity entityWithDictionary:dictionary[@"standard_resolution"]] forKey:@"standard_resolution"];
    [retVal setObject:[MiniessImageEntity entityWithDictionary:dictionary[@"thumbnail"]] forKey:@"thumbnail"];
    return retVal;
}


@end


@implementation MiniessImageEntity

+ (id) entityWithDictionary:(NSDictionary *)dictionary{
    MiniessImageEntity *entity = [[MiniessImageEntity alloc] init];
    entity.url = dictionary[@"url"];
    entity.width = [dictionary[@"width"] intValue];
    entity.height = [dictionary[@"height"] intValue];
//    [entity downloadImageWithBlock:nil];
    
    return entity;
}


- (void) downloadImageWithBlock:(void (^)(UIImage *image, NSError * error))block{
    
    [MiniessModel downloadImageWithUrl:self.url andBlock:^(UIImage *image2, NSError *error) {
        if (image2 && !error) {
            
            if (block) {
                block(image2, nil);
            }
        }
    }];
}



@end

@implementation MiniessMediaPagingEntity

+ (id) entityWithDataDictionary:(NSDictionary *)dataDictionary andPagingDictionary:(NSDictionary *)pagingDictionary{
    MiniessMediaPagingEntity *entity = [[MiniessMediaPagingEntity alloc] init];
    entity.nextUrl = pagingDictionary[@"next_url"];
    entity.mediaEntity = [MiniessMediaEntity entityWithDictionary:dataDictionary];
    
    return entity;
}

@end


