//
//  Config.h
//  MiniessExample
//
//  Created by Trung on 3/29/14.
//  Copyright (c) 2014 Busta. All rights reserved.
//

#ifndef MiniessExample_Config_h
#define MiniessExample_Config_h

#define RALE_WAY_REGULAR(s) [UIFont fontWithName:@"Raleway-Regular" size:s]
#define RALE_WAY_BOLD(s) [UIFont fontWithName:@"Raleway-Bold" size:s]
//change cells color here
#define CELL_COLORS @[[UIColor colorWithRed:27.0f/255.0f green:191.0f/255.0f blue:161.0f/255.0f alpha:1.0f],[UIColor colorWithRed:126.0f/255.0f green:113.0f/255.0f blue:128.0f/255.0f alpha:1.0f],[UIColor colorWithRed:255.0f/255.0f green:79.0f/255.0f blue:75.0f/255.0f alpha:1.0f],[UIColor colorWithRed:150.0f/255.0f green:214.0f/255.0f blue:217.0f/255.0f alpha:1.0f],[UIColor colorWithRed:230.0f/255.0f green:213.0f/255.0f blue:143.0f/255.0f alpha:1.0f]]
#endif
